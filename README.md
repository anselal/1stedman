<center>

# Unix Programmer's Manual
## November 3, 1971

</center>

The first edition of the Unix Programmer's Manual, dated November 3, 1971, is available here in image, Postscript, and PDF format. Return with us to the golden days of yesteryear!

The image links refer to PDF files produced by the scanner. They are fairly large, ranging up to a couple of MB, and are named **.pdf**. You may need a plugin or other software in order to view them; Adobe Acrobat is readily available.

The Postscript files are the result of OCR, are relatively modest in size (up to about 135KB) and should be suitable for viewing with a PS viewer or sending to a printer. They have been hand-edited by me in an attempt to make them look the same as they originally did. There are also PDF renditions distilled from the Postscript after OCR; they are smaller than the PS versions but have the same content.

OCR technology does a creditable job on reading plain typewritten text and in finding the page layout, and a moderately good one in inferring what's likely to be adjustable (as opposed to tabular) text. Underlining, particularly when combined with quote marks on the next line, is hard. The page layout in the PS version is like that of the original, except that adjustment of words across lines uses some combination of the OCR program's guess and hand repair of its misapprehensions. Substantive differences owe either to errors in the OCR that I failed to correct, or those introduced by me.

The scanning and OCR was done by the Textbridge package to create **.tif** files and also Word **.doc** files that I edited in Word; the PS was produced from Word 7.0\. I'd like to make good HTML, but the tools I've tried so far are inadequate. The PDF was created later (from the **.tif**), but also with Textbridge.

The original manuals were typed on a Model 37 Teletype terminal that was evidently not in perfect tune. No contemporary digital representation (e.g. tape) survives, so far as I know. Nor do we have an original paper copy of it. If you look at the title page reproduced here in the image format, you'll see that the real ink-and-hammer version belonged to Joe Ossanna and is marked in his hand. The paper on which this version was printed was given to Mel Ferentz on his retirement from Usenix, and the rendition here is from a xerographic copy of Ossanna's original, made just before the gift to Mel.

I don't know the whereabouts of any true "first edition" of the manual, in the sense of one of the copies in Bell System covers that we made for local distribution. Ken Thompson or someone else might conceivably have one secreted somewhere; I do not. Likewise, although we do have paper copies of the second through fourth edition manuals, I don't know of any digital version.

When I look over this manual, I'm amazed by a lot of things: first, by what has survived so long (the semantics of important system calls, and the important early commands) and also by what wasn't there. Neither pipes nor grep are in it, for example. Even B was not central (the manual apologizes that the B compiler is callable only by a shell script); C was still to come.

We even anticipated the millenium bug: time was measured in sixtieths of a second since 1 Jan. 1971 as a 32 bit quantity. The BUGS section for time(II) remarks, "The cronological-minded reader will note that 2**32 sixtieths of a second is only about 2.5 years." Later, this was patched more than once by declaring a new epoch, then again in 1973 by making the units full seconds dating from the 1970 New Year--this is the "classical" Unix epoch. Of course, it only pushed the issue off to 2038. Yet, the <u>cal</u> program even in 1971 knew about the hanky-panky in 1752!.

On the other hand, Ken had already written a chess-playing program, and we had done most of a Fortran compiler.

In 1971, when this manual was written, we had moved off the original PDP-7 to the PDP-11/20; it had 24KB of core memory, and no memory management hardware at all. The operating system and most of the software was still in assembly language, (rewritten or new, not "ported"). By this time we knew about the upcoming PDP-11/45, and had visited Digital in Maynard to talk about it; in particular, we had the specs for the floating-point instructions it supported. So the system described here included a simulator for the instructions (fptrap(III)).

* * *

Here's what is available for downloading now.

Lucent Technologies retains copyright on this material.

#### HTML:

*   [manintro.html](manintro.html), the Title page and Introduction

#### The manual contents, in post-OCR PostScript or PDF:

*   [manintro.ps](manintro.ps), [manintro.pdf](manintro.pdf), Title page and Introduction
*   [man11.ps](man11.ps), [man11.pdf](man11.pdf), Commands, part 1
*   [man12.ps](man12.ps), [man12.pdf](man12.pdf), Commands, part 2
*   [man13.ps](man13.ps), [man13.pdf](man13.pdf), Commands, part 3
*   [man14.ps](man14.ps), [man14.pdf](man14.pdf), Commands, part 4
*   [man21.ps](man21.ps), [man21.pdf](man21.pdf), System calls, part 1
*   [man22.ps](man22.ps), [man22.pdf](man22.pdf), System calls, part 2
*   [man31.ps](man31.ps), [man31.pdf](man31.pdf), Library routines
*   [man41.ps](man41.ps), [man41.pdf](man41.pdf), Special files
*   [man51.ps](man51.ps), [man51.pdf](man51.pdf), File formats
*   [man61.ps](man61.ps), [man61.pdf](man61.pdf), User-maintained software
*   [man71.ps](man71.ps), [man71.pdf](man71.pdf), Miscellaneous

#### The manual contents, in gzipped post-OCR PostScript:

*   [manintro.ps.gz](manintro.ps.gz.ps), Title page and Introduction
*   [man11.ps.gz](man11.ps.gz.ps), Commands, part 1
*   [man12.ps.gz](man12.ps.gz.ps), Commands, part 2
*   [man13.ps.gz](man13.ps.gz.ps), Commands, part 3
*   [man14.ps.gz](man14.ps.gz.ps), Commands, part 4
*   [man21.ps.gz](man21.ps.gz.ps), System calls, part 1
*   [man22.ps.gz](man22.ps.gz.ps), System calls, part 2
*   [man31.ps.gz](man31.ps.gz.ps), Library routines
*   [man41.ps.gz](man41.ps.gz.ps), Special files
*   [man51.ps.gz](man51.ps.gz.ps), File formats
*   [man61.ps.gz](man61.ps.gz.ps), User-maintained software
*   [man71.ps.gz](man71.ps.gz.ps), Miscellaneous

#### The manual in (big) image format.

On 11 Jan 1999, these files were converted from TIFF format to the more generally portable (and somewhat smaller) PDF. I still have the TIFF versions.

If you would prefer to download via ftp, these files are available in <tt>/cm/cs/who/dmr/pdfs</tt>, using anonymous ftp to <tt>plan9.bell-labs.com</tt>.

*   [manintro.pdf](pdfs/manintro.pdf), Title page and introduction.
*   [manindex.pdf](pdfs/manindex.pdf), Table of contents and permuted index.
*   [man11.pdf](pdfs/man11.pdf), Commands, part 1
*   [man12.pdf](pdfs/man12.pdf), Commands, part 2
*   [man13.pdf](pdfs/man13.pdf), Commands, part 3
*   [man14.pdf](pdfs/man14.pdf), Commands, part 4
*   [man21.pdf](pdfs/man21.pdf), System calls, part 1
*   [man22.pdf](pdfs/man22.pdf), System calls, part 2
*   [man31.pdf](pdfs/man31.pdf), Library routines
*   [man41.pdf](pdfs/man41.pdf), Special files
*   [man51.pdf](pdfs/man51.pdf), File formats
*   [man61.pdf](pdfs/man61.pdf), User-maintained software
*   [man71.pdf](pdfs/man71.pdf), Miscellaneous